﻿using TestProject.Interface;

namespace TestProject
{
    public class FactoryEncoder
    {
        private IEncoder data;
        public FactoryEncoder(IEncoder factory)
        {
            data = factory;
        }

        public string EncodeString(string str)
        {
            return data.EncodeString(str);
        }
    }
}
