﻿namespace TestProject.Interface
{
    public interface IEncoder
    {
        string EncodeString(string str);
    }
}