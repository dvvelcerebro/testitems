﻿using System;
using TestProject.Encoders;

namespace TestProject
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите sha1 если хотите использовать кодирования SHA-1 или нажмите Enter для использования по умолчанию");
            var readCoder = Console.ReadLine();

            var en = new FactoryEncoder(new Encoder());
            
            if (!string.IsNullOrEmpty(readCoder) && readCoder.ToLower().Equals("sha1"))
            {
                en = new FactoryEncoder(new EncoderSha());
                Console.WriteLine("Будет использована кодирование SHA-1");
            }
            else
            {
                Console.WriteLine("Будет использована кодирование по умолчанию");
            }
            Console.WriteLine("Введите пароль");

            var pass = Console.ReadLine();

            var passEncode = en.EncodeString(pass);
            Console.WriteLine(passEncode);

            Console.ReadLine();
        }


        public static string Reverse(string data)
        {
            char[] arr = data.ToCharArray();
            int i = 0;
            int j = arr.Length - 1;

            while (i < j)
            {
                char temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }

            return new string(arr);
        }
    }
}
