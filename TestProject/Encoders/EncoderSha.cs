﻿using System.Security.Cryptography;
using System.Text;
using TestProject.Interface;

namespace TestProject.Encoders
{
    public class EncoderSha : IEncoder
    {
        public string EncodeString(string str)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(str));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    sb.Append(b.ToString("X2"));
                }

                return sb.ToString();
            }
        }
    }
}
