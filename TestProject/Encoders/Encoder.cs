﻿using TestProject.Interface;

namespace TestProject.Encoders
{
    public class Encoder : IEncoder
    {
        public string EncodeString(string str)
        {
            string res = "";
            foreach (char ch in str)
            {
                char s = ch;
                s++;
                res += s;
            }
            return res;
        }
    }
}
